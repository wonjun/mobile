/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  NavigatorIOS,
  TabBarIOS,
  AlertIOS
} from 'react-native';
import styles from './styles';
import MediaListView from './media-list-view'
import ProductView from './product-view'
import Icon from 'react-native-vector-icons/Ionicons';

StatusBar.setBarStyle('light-content');

class mobile extends Component {
  constructor() {
    super();
    this.state = {selectedTab: 'today'}
  }
  setTab(tabId){
    this.setState({selectedTab: tabId})
  }
  render() {
    // https://github.com/oblador/react-native-vector-icons
    // http://ionicons.com/
    return (
      <TabBarIOS>
        <Icon.TabBarItem
          title="Today"
          iconName="ios-paper-outline"
          selectedIconName="ios-paper"
          selected={this.state.selectedTab === 'today'}
          onPress={() => this.setTab('today')}
          >
          <NavigatorIOS
            style={styles.global.mainContainer}
            barTintColor='#2A3744'
            tintColor='#EFEFEF'
            titleTextColor='#EFEFEF'
            initialRoute={{
              component: ProductView,
              title: 'Products',
          }}/>
        </Icon.TabBarItem>
        <Icon.TabBarItem
          title="Search"
          iconName="ios-search-outline"
          selectedIconName="ios-search"
          selected={this.state.selectedTab === 'search'}
          onPress={() => this.setTab('search')}>
          <NavigatorIOS
            style={styles.global.mainContainer}
            barTintColor='#2A3744'
            tintColor='#EFEFEF'
            titleTextColor='#EFEFEF'
            initialRoute={{
              component: MediaListView,
              title: 'Search',
            }}/>
        </Icon.TabBarItem>
        <Icon.TabBarItem
          title="Activity"
          iconName="ios-flash-outline"
          selectedIconName="ios-flash"
          selected={this.state.selectedTab === 'activity'}
          onPress={() => this.setTab('activity')}
          >
          <View style={styles.tabContent}>
            <Text>Activity View</Text>
          </View>
        </Icon.TabBarItem>
        <Icon.TabBarItem
          title="Profile"
          iconName="ios-person-outline"
          selectedIconName="ios-person"
          selected={this.state.selectedTab === 'profile'}
          onPress={() => this.setTab('profile')}
          >
          <View style={styles.tabContent}>
            <Text>Profile View</Text>
          </View>
        </Icon.TabBarItem>
      </TabBarIOS>
    );
  }
}

AppRegistry.registerComponent('mobile', () => mobile);