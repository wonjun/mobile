import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import global from './styles/global';
import listView from './styles/list-view';

export default {
    global: global,
    listView: listView
}