https://getexponent.com/

CODES
about/index.js in product kitty a few components
https://github.com/akveo/react-native-reddit-reader
loading compoenents
https://github.com/rkho/product-kitty
https://github.com/catalinmiron/react-native-dribbble-app
https://github.com/catalinmiron?tab=repositories
https://github.com/catalinmiron/movies

https://www.raywenderlich.com/126063/react-native-tutorial
https://github.com/voximplant/react-native-demo

Activity View
https://github.com/naoufal/react-native-activity-view

Tabbar
http://richardkho.com/persisting-tabbars-in-react-native/
https://egghead.io/lessons/react-react-native-basic-ios-routing
https://egghead.io/series/react-native-fundamentals
http://richardkho.com/persisting-tabbars-in-react-native/
https://devdactic.com/react-native-tab-bar/
https://www.npmjs.com/package/react-native-tab-navigator
https://www.youtube.com/watch?v=MUzNfTvnsXk
http://blog.thebakery.io/react-native-experimental-navigation-with-redux/
http://herman.asia/building-a-flashcard-app-with-react-native
https://www.smashingmagazine.com/2016/04/the-beauty-of-react-native-building-your-first-ios-app-with-javascript-part-1/
https://github.com/catalinmiron/react-native-scrollable-tab-view


https://github.com/facebook/react-native/tree/master/Examples/Movies
https://github.com/facebook/react-native/tree/master/Examples/UIExplorer
https://github.com/facebook/react-native/tree/master/Examples/TicTacToe
https://github.com/facebook/react-native/tree/master/Examples/2048

Auto0
https://auth0.com/docs/client-platforms/react
https://auth0.com/docs/server-apis/nodejs
https://auth0.com/docs/quickstart/native-mobile/react-native-ios/nodejs

Youtube
https://js.coach/react-native/react-native-youtube

Code Quality
https://yannick.cr/
CODE QUALITY WITH JSCS AND ESLINT
https://github.com/yannickcr/eslint-plugin-react

Source Code
https://github.com/sonnylazuardi/ziliun-react-native
https://github.com/bartonhammond/snowflake
https://github.com/7kfpun/FinanceReactNative

API Tutorials
https://auth0.com/docs/client-platforms/react
https://www.udemy.com/react-js/?siteID=IqQtCJUA_uE-7uO5rsIoe4HomO_5JfRorw&LSNPUBID=IqQtCJUA/uE
https://www.udemy.com/learn-and-understand-reactjs/?siteID=IqQtCJUA_uE-NYXM5MQJy8mJoaIRVgCvTQ&LSNPUBID=IqQtCJUA/uE

ReactJS Tutorials
https://www.udemy.com/reactnative/?siteID=IqQtCJUA_uE-nG2BKoDkqb5T1Nx1N8GfIw&LSNPUBID=IqQtCJUA/uE
http://www.reactnativeexpress.com/
http://www.reactnative.com/uiexplorer/

Checkout
https://itunes.apple.com/ca/app/facebook-groups/id931735837?mt=8&ign-mpt=uo%3D4
(IOS) FACEBOOK ADS MANAGER
F8

Features
https://github.com/jsdf/ReactNativeHackerNews
https://js.coach/react-native/react-native-refreshable-listview?sort=popular
https://js.coach/react-native/react-native-scrollable-tab-view?sort=popular
https://js.coach/react-native/react-native-vector-icons?sort=popular
https://js.coach/react-native/react-native-navbar?sort=popular
https://js.coach/react-native/react-native-image-picker?sort=popular&page=2
https://js.coach/react-native/react-native-app-intro?sort=popular&page=4
https://js.coach/react-native/react-native-social-share?sort=popular&page=6
https://js.coach/react-native/react-native-youtube?sort=popular&page=6
https://www.thepolyglotdeveloper.com/2015/10/implement-a-barcode-scanner-using-react-native/
http://blog.sendbird.com/tutorial-build-a-messaging-app-using-react-native/
https://js.coach/react-native/react-native-camera-roll-picker
https://www.thepolyglotdeveloper.com/2015/09/use-the-ios-camera-in-your-react-native-mobile-app/

Editor
https://nuclide.io/docs/quick-start/getting-started/

CSS
https://github.com/GeekyAnts/NativeBase?utm_campaign=explore-email&utm_medium=email&utm_source=newsletter&utm_term=daily

Artwork
Custom Launch Screen - https://www.youtube.com/watch?v=mS3CH_bKabw
images 1024 x 1024 remove black bg
Create gorgeous images for the App Store - https://launchkit.io/screenshots/onboard/

Submission
Apphub
https://github.com/fastlane/fastlane

React Native Modules
https://js.coach/react-native

Push notifications
https://github.com/catalinmiron/node-pushserver

Photo Viewer
https://github.com/NYTimes/NYTPhotoViewer

More
https://github.com/enaqx/awesome-react
https://github.com/naoufal/awesome-react-native

Web
https://www.openhunt.co/

Node API
http://jdlm.info/articles/2016/03/06/lessons-building-node-app-docker.html?r=0