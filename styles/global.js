import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  content: {
    flex: 1,
    backgroundColor: '#EFEFEF'
  },
  navbar: {
    backgroundColor: '#303030',
    paddingTop: 30,
    paddingBottom: 10,
    flexDirection: 'row'
  },
  tabContent: {
    flex: 1,
    alignItems: 'center'
  },
  tabText: {
    margin: 50,
    fontSize: 45
  }
});